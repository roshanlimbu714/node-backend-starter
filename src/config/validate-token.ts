import {NextFunction, Request, Response} from "express";

const jwt = require("jsonwebtoken");
// middleware to validate token
export const verifyToken = (req: Request, res: Response, next: NextFunction) => {
    const token = req.header("auth-token");
    if (!token) return res.status(401).json({error: "Access denied"});
    try {
        req.body.user = jwt.verify(token, process.env.JWT_SECRET);
        next(); // to continue the flow
    } catch (err) {
        res.status(400).json({error: "Token is not valid"});
    }
};
