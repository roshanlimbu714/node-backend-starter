import express from 'express';
import mongoose from 'mongoose';
import bodyParser, {json} from 'body-parser';
import {openRoutes, protectedRoutes} from './modules';
import {db} from "./config/config";
import path from "path";
import cookieParser from "cookie-parser";
import {verifyToken} from "./config/validate-token";

require('dotenv').config();

const app = express();
app.use(json());

app.listen(3000, () => {
    console.log('Server is listening on port 3000')
});
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(cookieParser());
app.use((req, res, next) => { //doesn't send response just adjusts it
    res.header("Access-Control-Allow-Origin", "*") //* to give access to any origin
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization" //to give access to all the headers provided
    );
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET'); //to give access to all the methods provided
        return res.status(200).json({});
    }
    app.use('/', (req, res) => {
        res.send('working');
    });
    next(); //so that other routes can take over
})

Object.values(openRoutes).map(route => app.use('/api', route));
// Object.values(protectedRoutes).map(route => app.use('/api',verifyToken, route));

mongoose.connect(db, {useNewUrlParser: true}).then((_) => {
    console.log('Connected to DB');
}).catch(err => {
    console.log(err);
});


