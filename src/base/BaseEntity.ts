import express from 'express';
import {validateData} from "../utils/validation";
import {modelController} from './BaseController';
export class BaseEntity {
    public path:string= '';
    public router = express.Router();
    public validationRules;
    public modelDTO;
    public model;
    public modelController = modelController;

    constructor(path:string, model: any, validationRules:any, modelDTO:any) {
        this.path = path;
        this.model = model;
        this.validationRules = validationRules;
        this.modelDTO = modelDTO;
    }


    getAllModel=async (request: express.Request, response: express.Response, next:express.NextFunction)=>{
        try{
            const dataJSON = await this.modelController.getAllData(this.model);
            const collection = dataJSON.map((v:any)=>this.modelDTO.sender(v));
            response.status(200).json({
                status: 400,
                data: collection,
                message: 'Data returned successfully'
            })
        }catch(e){
            response.status(400).json({
                status: 400,
                message: 'Data could not be retraieved'
            },)
        }
    }

    getModelByName = async (request: express.Request, response: express.Response, next:express.NextFunction) =>{
        try{
            const dataJSON = await this.modelController.getDataByName(this.model,request.params.name);
            const collection = this.modelDTO.sender(dataJSON);
            response.status(200).json({
                status: 400,
                data: collection,
                message: 'Data returned successfully'
            })
        }catch(e){
            console.log(e)
            response.status(400).json({
                status: 400,
                message: 'Data could not be retrieved by name'
            },)
        }
    }

    getModelById=async (request: express.Request, response: express.Response, next:express.NextFunction)=>{
        try{
            const dataJSON = await this.modelController.getDataById(this.model,request.params.id);
            const collection = dataJSON.map((v:any)=>this.modelDTO.sender(v));
            response.status(200).json({
                status: 400,
                data: collection,
                message: 'Data returned successfully'
            })
        }catch(e){
            response.status(400).json({
                status: 400,
                message: 'Data could not be retrieved by id'
            },)
        }
    }

     createModel=async (request: express.Request, response: express.Response)=>{
         console.log(request.body);
        try {
             const modelData = request.body;
             const isValidated = validateData(this.validationRules, modelData)
             if (isValidated === '') {
                 const modelJSONCreate = this.modelDTO.receiver(modelData);
                 await this.modelController.createData(this.model,modelJSONCreate);
                 return response.json({
                     status: 200,
                     data: this.modelDTO.sender(modelJSONCreate),
                     message: "Date created successfully"
                 })
             }
             return response.json({
                 status: 400,
                 data: [],
                 message: isValidated
             });
         } catch (err) {
             return response.json({
                 status: 400,
                 message: err.message
             })
         }
    }

    deleteModel =async (request: express.Request, response: express.Response, next:express.NextFunction)=>{
        try{
            const dataJSON = await this.modelController.deleteData(this.model,request.params.id);
            response.status(200).json({
                status: 400,
                message: 'Data deleted successfully'
            })
        }catch(e){
            response.status(400).json({
                status: 400,
                message: 'Data could not be deleted'
            },)
        }
    }


}