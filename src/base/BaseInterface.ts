export interface IBaseInterface {
    _id: String;
    createdAt: String;
    updatedAt: String;
}