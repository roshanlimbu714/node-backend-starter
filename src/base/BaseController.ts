import Transaction from "../modules/transaction/transaction.schema";

export const modelController = {
    getAllData:(model:any) => {
        return model.find({});
    },
    createData:async (model:any,data:any) => {
           const session = await model.startSession();
           await session.withTransaction((): Promise<any> => {
               return model.create([data],{session:session});
           });
           session.endSession();
    },
    deleteData:async (model:any, id:any)=>{
        return model.deleteOne({_id:id});
    },
    getDataById(model:any,id:String){
        return model.find({_id:id});
    },
    getDataByName(model:any,name:String){
        return model.findOne({name:name});
    }
}