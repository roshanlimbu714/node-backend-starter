import * as mongoose from 'mongoose';

const materialSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    quantity: {
        type: Number,
        required: true
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'materialCategory'
    }
}, {timestamps: true});

export default mongoose.model('MaterialSchema', materialSchema);