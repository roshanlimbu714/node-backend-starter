export const materialRules = {
    name: {
        required: true,
        isNumber: true,
        label: 'Name'
    },
    quantity: {
        minLength: 6,
        type: 'Number',
        maxLength: 9,
        required: true,
        label: 'Quantity'
    },
    category: {
        required: true,
        label: 'Category'
    },
}