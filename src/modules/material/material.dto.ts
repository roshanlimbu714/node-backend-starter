import moment from 'moment';
import {IBaseInterface} from '../../base/BaseInterface';

export interface IMaterial extends IBaseInterface {
    name: String;
    quantity: String;
    category: String
}

export const MaterialDto = {
    receiver: (material: IMaterial) => {
        return {
            name: material.name,
            quantity: material.quantity,
            category: material.category
        }
    },
    sender: (material: IMaterial) => {
        return {
            id: material._id,
            createdAt: material.createdAt,
            updatedAt: material.updatedAt,
            name: material.name,
            quantity: material.quantity,
            category: material.category
        }
    }
}
