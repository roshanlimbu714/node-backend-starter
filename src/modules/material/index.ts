import {BaseEntity} from "../../base/BaseEntity";
import MaterialSchema from './material.schema';
import {materialRules } from './material.validator';
import {MaterialDto} from "./material.dto";
import {materialController} from './material.controller';

export class Material extends BaseEntity{
    constructor() {
        super('/material', MaterialSchema, materialRules, MaterialDto);
        this.initializeRoutes();
    }

   private initializeRoutes(){
       this.router.get(this.path, this.getAllModel);
       this.router.post(this.path, this.createModel);
       this.router.delete(this.path, this.deleteModel);
   }

}

