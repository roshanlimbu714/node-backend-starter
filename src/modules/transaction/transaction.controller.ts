import Transaction from "./transaction.schema";
import express from "express";

export const transactionController = {
    getAllData:async (response: express.Response) => {
        return Transaction.find({}).populate('recipient');
    },
}