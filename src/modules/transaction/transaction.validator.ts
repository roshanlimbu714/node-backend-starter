export const transactionRules = {
    date: {
        required: true,
        isNumber:true,
        label: 'Date'
    },
    particulars: {
        required:true,
        label: 'Particulars '
    },
    companyName:{
        required:true,
        label: 'Company Name'
    },
    totalAmount: {
        required:true,
        isNumber:true,
        label: 'Total Amount'
    },
    transactionType: {
        required:true,
        label: 'Transaction Type'
    },
    remarks: {
        label: 'Remarks'
    }

}