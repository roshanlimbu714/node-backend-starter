import moment from 'moment';
import {IBaseInterface} from '../../base/BaseInterface';

export interface ITransaction extends IBaseInterface{
    date : String;
    particulars : String;
    totalAmount : String;
    transactionType : String;
    recipient : String;
    itemList : String;
    remarks: String;
}

export const TransactionDto = {
    receiver: (transaction: ITransaction) => {
        return {
            date: new Date(transaction.date.toString()),
            particulars: transaction.particulars,
            totalAmount: Number(transaction.totalAmount),
            transactionType: transaction.transactionType,
            recipient: transaction.recipient,
            remarks: transaction.remarks
        }
    },
    sender: (transaction: ITransaction) => {
        return {
            date: moment(new Date(transaction.date.toString())).format('DD MMM, YYYY'),
            particulars: transaction.particulars,
            totalAmount: Number(transaction.totalAmount),
            transactionType: transaction.transactionType,
            recipient: transaction.recipient,
            remarks: transaction.remarks
        }
    }
}
