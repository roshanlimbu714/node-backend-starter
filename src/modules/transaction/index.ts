import {BaseEntity} from "../../base/BaseEntity";
import express from "express";
import TransactionSchema from './transaction.schema';
import {transactionRules} from './transaction.validator';
import {TransactionDto} from "./transaction.dto";
import {transactionController} from "./transaction.controller";

export class Transaction extends BaseEntity {
    constructor() {
        super('/transaction', TransactionSchema, transactionRules, TransactionDto,);
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(this.path, this.getAllModel);
        this.router.post(this.path, this.createModel);
        this.router.delete(this.path, this.deleteModel);
    }
}

