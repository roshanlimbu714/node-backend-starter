import * as mongoose from 'mongoose';

const transactionSchema = new mongoose.Schema({
    date: {
        type: Date,
        required: true,
    },
    particulars: {
        type: String,
        required: true,
    },
    totalAmount: {
        type: String,
        required: true,
    },
    transactionType: {
        type: String,
        required: true
    },
    recipient: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'recipient'
    },
    itemList: {
        type: mongoose.Schema.Types.Array,
        required: true,
        ref: 'orderItem'
    }
},{timestamps: true});

export default mongoose.model('TransactionSchema', transactionSchema);