import * as jwt from 'jsonwebtoken';
import User from "../user/user.schema";


const authMiddleWare = async (request: any, response: any, next: any) => {
    const cookies = request.cookies;
    if (cookies?.Authorization) {
        const secret = process.env.JWT_SECRET as string;
        try {
            let verificationResponse: any;
            // @ts-ignore
            verificationResponse = jwt.verify(cookies.Authorization, secret);
            const id = verificationResponse._id;
            const user = await User.findById(id);
            if (user) {
                request.user = user;
                next();
            }
        } catch (error) {
            console.log(error);
        }
    }
}

export default authMiddleWare;