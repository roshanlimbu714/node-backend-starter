import express from "express";
import {loggingIn, register} from "./auth.controller";


const router = express.Router();

router.post('/auth/register', register)
router.post('/auth/login', loggingIn)

export { router as authRouter};
