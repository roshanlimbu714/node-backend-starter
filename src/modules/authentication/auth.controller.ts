import express from "express";
import User from "../user/user.schema";
import {IDataStoredInToken, ITokenData, IUser} from "../user/user.interface";
import * as jwt from 'jsonwebtoken';

export const register = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const userData = req.body;
    if (await User.findOne({email: userData.email})) {
        res.status(400).send({status: 400, errorMessage: "username already exists"});
        next();
    } else {
        const user = await User.create(userData);
        res.send(user);
    }
}

export const loggingIn = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const logInData = request.body;
    const user = await User.findOne({email: logInData.email});
    if (user) {
        const isPasswordMatching = user.password === logInData.password;
        if (isPasswordMatching) {
            const tokenData = createToken(user);
            console.log(tokenData, "**********Token Data**********");
            response.setHeader('Set-Cookie', [createCookie(tokenData)]);
            response.send(user);
        } else {
            response.status(400).send({status: 400, errorMessage: "wrong credentials"});
        }
    } else {
        response.status(400).send({status: 400, errorMessage: "wrong credentials"});
    }
}

const createToken = (user: any): ITokenData => {
    const expiresIn = 60 * 60; // an hour
    const secret = process.env.JWT_SECRET as string;
    const dataStoredInToken: IDataStoredInToken = {
        _id: user._id,
    };
    // @ts-ignore
    return {
        expiresIn,
        token: jwt.sign(dataStoredInToken, secret, {expiresIn}),
    };
}

const createCookie = (tokenData: ITokenData) => {
    return `Authorization=${tokenData.token}; HttpOnly; Max-Age=${tokenData.expiresIn}`;
}