import {Recipient} from './recipient';
import {authRouter} from "./authentication";
import {Transaction} from "./transaction";
import {Material} from "./material";
import {Category} from "./category";
import {Customer} from "./customer";

export const openRoutes = {
     recipient: new Recipient().router,
     auth: authRouter,
     transaction: new Transaction().router,
     material: new Material().router,
     category: new Category().router,
     customer: new Customer().router
}

export const protectedRoutes = {
}