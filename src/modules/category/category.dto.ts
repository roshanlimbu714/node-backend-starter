import {IBaseInterface} from '../../base/BaseInterface';

export interface ICategory extends IBaseInterface {
    name: String;
}

export const CategoryDTO = {
    receiver: (category: ICategory) => {
        return {
            name: category.name,
        }
    },
    sender: (category: ICategory) => {
        return {
            id: category._id,
            createdAt: category.createdAt,
            updatedAt: category.updatedAt,
            name: category.name,
        }
    },
    sendCollection: (customerArr:any) => {
        return customerArr.map((v:any) => v.name);
    },
}
