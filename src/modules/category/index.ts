import {BaseEntity} from "../../base/BaseEntity";
import CategorySchema from './category.schema';
import {categoryRules } from './category.validator';
import {CategoryDTO} from "./category.dto";
import {categoryController} from './category.controller';
import express from "express";

export class Category extends BaseEntity{
    constructor() {
        super('/category', CategorySchema, categoryRules, CategoryDTO);
        this.initializeRoutes();
    }

   private initializeRoutes(){
       this.router.get(this.path, this.getAllModel);
       this.router.post(this.path, this.createModel);
       this.router.get(this.path+'/collec', this.getCategoryCollection);
       this.router.get(this.path + '/name/:name', this.getModelByName);
       this.router.get(this.path + '/:id', this.getModelById);
       this.router.delete(this.path+ '/:id', this.deleteModel);
   }

   getCategoryCollection = async (request: express.Request, response: express.Response, next:express.NextFunction)=>{
       try{
           const dataJSON = await this.modelController.getAllData(this.model);
           const collection = this.modelDTO.sendCollection(dataJSON);
           response.status(200).json({
               status: 400,
               data: collection,
               message: 'Data returned successfully'
           })
       }catch(e){
           console.log(e);
           response.status(400).json({
               status: 400,
               error:e,
               message: 'Data could not be retrieved of collection'
           },);
       }
   }
}

