export interface ITokenData {
    token: string;
    expiresIn: number;
}

export interface IDataStoredInToken {
    _id: string;
}

export interface IUser {
    name: String,
    email: String,
    password: String,
}
