import * as mongoose from 'mongoose';
import { IUser } from './user.interface';

const userSchema = new mongoose.Schema({
    name: String,
    email: String,
    password: String

});


const User = mongoose.model<IUser & mongoose.Document>('user', userSchema);

export default User;