import * as mongoose from 'mongoose';

const recipientSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: true,
    },
    balance: {
        type: Number,
        required: true,
        ref: 'recipient'
    },
    companyName: {
        type: String,
        required: true,
        ref: 'recipient'
    },
    lastPaid: {
        type: String,
        required: true,
    }
}, {timestamps: true});

export default mongoose.model('RecipientSchema', recipientSchema);