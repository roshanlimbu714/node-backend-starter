import {BaseEntity} from "../../base/BaseEntity";
import RecipientSchema from './recipient.schema';
import {recipientRules} from './recipient.validator';
import {RecipientDTO} from "./recipient.dto";
import {recipientController} from './recipient.controller';

export class Recipient extends BaseEntity{
    constructor() {
        super('/recipient', RecipientSchema, recipientRules, RecipientDTO);
        this.initializeRoutes();
    }

   private initializeRoutes(){
       this.router.get(this.path, this.getAllModel);
       this.router.post(this.path, this.createModel);
       this.router.delete(this.path+'/:id', this.deleteModel);
       this.router.get(this.path+'/:id', this.getModelById);
   }

}

