import moment from 'moment';
import {IBaseInterface} from '../../base/BaseInterface';
export interface IRecipient  extends IBaseInterface{
    name: String,
    phone: String,
    balance: Number,
    companyName: String,
    lastPaid: String
}

export const RecipientDTO = {
    receiver: (recipient: IRecipient) => {
        return {
            name: recipient.name,
            phone: recipient.phone,
            balance: Number(recipient.balance),
            companyName: recipient.companyName,
            lastPaid: new Date(recipient.lastPaid.toString())
        }
    },
    sender: (recipient: IRecipient) => {
        return {
            id: recipient._id,
            createdAt: recipient.createdAt,
            updatedAt: recipient.createdAt,
            name: recipient.name,
            phone: Number(recipient.phone),
            balance: Number(recipient.balance),
            companyName: recipient.companyName,
            lastPaid: moment(new Date(recipient.lastPaid.toString())).format('DD MMM, YYYY')
        }
    }
}
