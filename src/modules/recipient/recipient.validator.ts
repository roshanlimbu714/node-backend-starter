export const recipientRules = {
    name: {
        required: true,
        isNumber:true,
        label: 'Name'
    },
    phone: {
        minLength: 6,
        type: 'Number',
        maxLength:9,
        required:true,
        label: 'Phone'
    },
    companyName:{
        required:true,
        label: 'Company Name'
    },
    balance: {
        required:true,
        isNumber:true,
        type: 'Number',
        label: 'Balance'
    },
    lastPaid: {
        type: 'Date',
        required:true,
        label: 'Last Paid Date'
    }
}