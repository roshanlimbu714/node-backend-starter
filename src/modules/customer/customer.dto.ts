import {IBaseInterface} from '../../base/BaseInterface';

export interface ICustomer extends IBaseInterface {
    name: String;
    companyName: String;
    phone: Number;
    balance: Number;
}

export const CustomerDto = {
    receiver: (customer: ICustomer) => {
        return {
            name: customer.name,
            companyName: customer.companyName,
            phone: customer.phone,
            balance: customer.balance,
        }
    },
    sender: (customer: ICustomer) => {
        return {
            id: customer._id,
            createdAt: customer.createdAt,
            updatedAt: customer.updatedAt,
            name: customer.name,
            companyName: customer.companyName,
            phone: customer.phone,
            balance: customer.balance,
        }
    },
}
