import {BaseEntity} from "../../base/BaseEntity";
import CategorySchema from './customer.schema';
import {customerRules } from './customer.validator';
import {CustomerDto} from "./customer.dto";
import {customerController} from './customer.controller';

export class Customer extends BaseEntity{
    constructor() {
        super('/customer', CategorySchema, customerRules, CustomerDto);
        this.initializeRoutes();
    }

   private initializeRoutes(){
       this.router.get(this.path, this.getAllModel);
       this.router.post(this.path, this.createModel);
       this.router.get(this.path + '/:id', this.getModelById);
       this.router.get(this.path + '/:name', this.getModelByName);
       this.router.delete(this.path+ '/:id', this.deleteModel);
   }
}

