export const customerRules = {
    name: {
        required: true,
        label: 'Name'
    },
    balance: {
        isNumber: true,
        required: true,
        label: 'Balance'
    },
    companyName: {
        required: true,
        label: 'Company Name'
    },
    phone: {
        required: true,
        label: 'Phone'
    },
}