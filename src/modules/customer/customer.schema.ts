import * as mongoose from 'mongoose';

const customerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    companyName: {
        type: String,
        required: true,
        unique: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    balance: {
        type: String,
        required: true,
        unique: true
    },
}, {timestamps: true});

export default mongoose.model('CustomerSchema', customerSchema);