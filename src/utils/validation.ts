export const validateData = (rules: any, data: any) => {
    let message = '';
    Object.keys(data).forEach((v) => {
        if (rules[v].required && data[v] === '') {
            message += rules[v].label + ' is required. ';
        }

        if (rules[v].type === 'Number' && isNaN(data[v])) {
            message += rules[v].label + ' should be a number. ';
        }

        if (rules[v].type === 'Date') {
            const date = new Date(data[v]);
            const val = isNaN(Date.parse(date.toString()));
            if (val) {
                message += rules[v].label + ' should be valid. ';
            }
        }
        if (rules[v].minLength && data[v].length < rules[v].minLength) {
            message += rules[v].label + ' should be more than '+ rules[v].minLength +' characters. ';
        }

        if (rules[v].maxLength && data[v].length > rules[v].maxLength) {
            message += rules[v].label + ' should be less than '+ rules[v].maxLength +' characters. ';
        }
    });
    return message;
}